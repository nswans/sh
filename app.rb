require 'rubygems'
require 'bundler'
require 'json'
require 'securerandom'
require 'uri'

Bundler.require

require_rel 'models'

require_relative "./database.env.rb" unless (ENV["PRODUCTION"] == "YES")

class Shortener < Sinatra::Base
  require_rel 'helpers'

  set :bind, "0.0.0.0"
  set :port, 5000 unless (ENV["PRODUCTION"] == "YES")
  enable :sessions
  set :root, File.dirname(__FILE__)
  disable :raise_errors if (ENV["PRODUCTION"] == "YES")
  disable :show_exceptions if (ENV["PRODUCTION"] == "YES")
  set :haml, format: :html5

  register Sinatra::Namespace
  helpers Sinatra::RequiredParams, Authorization

  configure do
    MongoMapper.setup( {'production' => { 'uri' => ENV['DATABASE_URI'] } }, 'production')
  end

  get '/' do
    haml :index
  end

  get '/login' do
    haml :login
  end

  error 404 do
    haml :error
  end

  error 401 do
    haml :unauth
  end

  error 500 do
    haml :serr
  end

  get "/:encoded" do
    link = Link.all(slug: params[:encoded])[0]
    if link.nil? then halt 404 end
    redirect link[:url]
  end
end

require_relative './controllers/base_controller.rb'
require_rel 'controllers'

Shortener.run! if __FILE__==$0