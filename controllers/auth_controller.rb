class AuthController < BaseController
  post '/api/cookie/login' do
    login! params[:username], params[:password]
    redirect to("/")
  end
  
  post '/api/cookie/logout' do
    logout!
    redirect to("/")
  end
end