class AssetsController < BaseController
  namespace '/assets' do
    get '/style/*.*' do |file, ext|
      if ext=="scss"
        scss (("style/#{file}").to_sym)
      else
        `pwd`
        File.read("./views/style/#{file}.css")
        #(("style/#{file}}").to_sym)
      end
    end
  end
end
