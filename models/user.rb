class User
  include MongoMapper::Document

  key :username, String
  key :password_digest, String
  key :admin, Integer
end