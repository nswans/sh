module Authorization
  def authorized?
    !session[:userID].nil?
  end

  def login! username, password
    user = User.all(username: username)[0]
    pass = BCrypt::Password.new(user[:password_digest]) == password

    if pass
      status 200
      session[:userID] = username
      session[:userAdmin] = user[:admin]
    else
      status 401
      session[:userID] = nil
      session[:userAdmin] = nil    
      return "oops"
    end
  end

  def logout!
    session[:userID] = nil
    session[:userAdmin] = nil
  end

  def protected! admin = false
    if admin
      if (session[:userAdmin] == 0)
        redirect to('/login')
      end
    else
      if (session[:userID].nil?)
        redirect to('/login')
      end
    end
  end
end