class Link
  include MongoMapper::Document

  key :slug, String
  key :url, String
end