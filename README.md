# sh
`sh` is a simple shortener service backed by MongoDB and Sinatra

# instructions
`sh` needs an environment variable called `DATABASE_URI` as this is where is the database located.