Vue.component('info', {
  template: `
    <footer>
      <span>(C) Noah Swanson 2018-2019 | </span>
      <a class="uk-link-reset" href="mailto:noah@kome.ga">noah@kome.ga</a>
      <span> | </span>
      <a class="uk-link-reset" href="https://twitter.com/kkslaughter2070">@kkslaughter2070</a>
    </footer>
  `
})

app = new Vue({
  el: '#app',
  data: {
    message: 'Hell, World!'
  }
})

$('#linkContainer').hide()

$('#shorten').on('click', () => {
  longURL = $('#urlField').val()
  data = '{ "url": "'+longURL+'" }'
  console.log(data)

  $.ajax({
    url: "/api/v1/link",
    type: "POST",
    dataType: 'JSON',
    data: '{ "url": "'+longURL+'" }',
    success: (data) => {
      var result = '<p></p><div class="uk-alert-primary" uk-alert><a class="shortened uk-link-reset" href="'+data.data.url+'">'+data.data.url+'</a></div>'
      $('#linkContainer').fadeOut(250, () => {
        $("#linkContainer").html(result)
        $("#linkContainer").fadeIn()
      })
    },
    error: (jqXHR, exception) => {
      var message = eval('('+jqXHR.responseText+')').message
      var result = '<p></p><div class="uk-alert-danger" uk-alert><span class="shortened">'+message+'</a></div>'
      $('#linkContainer').fadeOut(250, () => {
        $("#linkContainer").html(result)
        $("#linkContainer").fadeIn()
      })
    }
  })

  console.log(data)
});